import "./App.css";
import InputComponent from "./component/InputComponent";
import background from "./img/background.png";
function App() {
  return (
    <div
      style={{ backgroundImage: `url(${background})` }}
      className="App"
      class="flex justify-center align-center items-center h-screen bg-cover "
    >
      <InputComponent />
      <div class="w-30 "></div>
    </div>
  );
}

export default App;
