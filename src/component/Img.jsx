import React from "react";
import background from "../img/background.png";

function Img() {
  return <div style={{ backgroundImage: `url(${background})` }}></div>;
}

export default Img;
