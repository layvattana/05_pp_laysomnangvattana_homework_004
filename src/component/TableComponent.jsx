import React, { Component } from "react";

export default class TableComponent extends Component {
  render() {
    return (
      // <div class="flex justify-center align-center item-center">
      //   <div class="relative overflow-x-auto shadow-md sm:rounded-lg mt-20 mb-10 flex-col shadow-black">
      //     {/* Heading */}
      //     <h2 className="font-bold text-2xl p-5 bg-lime-600 w-auto flex sm:align-center sm:justify-center sm:item-center">
      //       Dispaly Student Name
      //     </h2>
      //     {/* Table Dispaly */}
      //     <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
      //       <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
      //         <tr>
      //           <th scope="col" class="px-6 py-3">
      //             ID
      //           </th>
      //           <th scope="col" class="px-6 py-3">
      //             Student Name
      //           </th>
      //         </tr>
      //       </thead>
      //       <tbody>
      //         {this.props.data.map((item) => (
      //           <tr
      //             key={item.id}
      //             class="border-b bg-gray-50 dark:bg-gray-800 dark:border-gray-700"
      //           >
      //             <td class="px-6 py-4">{item.id}</td>
      //             <td class="px-6 py-4">{item.perP}</td>
      //           </tr>
      //         ))}
      //       </tbody>
      //     </table>
      //   </div>
      // </div>

      <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
          <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400 text-center">
            <tr>
              <th scope="col" class="px-6 py-3">
                ID
              </th>
              <th scope="col" class="px-6 py-3">
                EMAIL
              </th>
              <th scope="col" class="px-6 py-3">
                USERNAME
              </th>
              <th scope="col" class="px-6 py-3">
                AGE
              </th>
              <th className="flex align-center items-center text-center" scope="col" class="px-6 py-3"> Action
              </th>
            </tr>
          </thead>
          <tbody>
            {this.props.data.map((item) => (
              <tr
                class="bg-white border-b dark:bg-gray-900 dark:border-gray-700"
                key={item.id}
              >
                <td class="px-6 py-4">{item.id}</td>
                <td class="px-6 py-4">{item.perEmail}</td>
                <td class="px-6 py-4">{item.perP}</td>
                <td class="px-6 py-4">{item.perAge}</td>
                <td class="px-6 py-4">
                  <button
                    type="button"   onClick={()=>this.props.avc(item.id)}
                    class="focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900"
                  >
                    Pending
                  </button>
                  <button
                    type="button"
                    class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800"
                  >
                    Show more
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}
