import React, { Component } from "react";
import TableComponent from "./TableComponent";

export default class InputComponent extends Component {
  constructor() {
    super();
    this.state = {
      person: [],
      nPerson: "",
      email:[],
      nEmail: "",
      age: [],
      nAge: "",
      action: [],
      nAction: "",
    };
  }

  //   get Value from user input
  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,

    });
  };
  //   get Value from user input
  handleButton = (event) => {
    console.log(event)
    this.setState({
      [event.target.name]: event.target.value,
      
    });
  };

  //   Add new object to array

  handleSubmit = () => {
    const newObj = {
      id: this.state.person.length + 1,
      perP: this.state.nPerson,
      perEmail: this.state.nEmail,
      perAge: this.state.nAge,
      perAction: this.state.nAction
    };
    this.setState(
      {
        person: [...this.state.person, newObj],
      
        email: [...this.state.email, newObj],
      
        age: [...this.state.age, newObj],
     
        action: [...this.state.action, newObj],
      

      },
      () => console.log(this.state.person),
            console.log(this.state.email),
            console.log(this.state.age),
            console.log(this.state.action)
    );
  };

  render() {
    return (
      <div>
        <p className="text-white text-2xl text-center font-bold"> Please fill in your information</p>
        <div className="mt-10">
          <label
            for="input-group-1"
            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white text-white"
          >
            Your Email
          </label>
          <div class="relative mb-2">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
              <svg
                aria-hidden="true"
                class="w-5 h-5 text-gray-500 dark:text-gray-400"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path>
                <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
              </svg>
            </div>
            <input
              type="text"
              name="nEmail"
              id="input-group-1"
              class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="your@gmail.com"
              onChange={this.handleChange}
            ></input>
          </div>
          <label
            for="website-admin"
            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white text-white"
          >
            Username
          </label>
          <div class="flex">
            <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
              @
            </span>
            <input
              type="text"
              name="nPerson"
              id="website-admin"
              class="rounded-none rounded-r-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full  text-sm border-gray-300 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Username"
              onChange={this.handleChange}
            ></input>
          </div>

          <label
            for="website-admin"
            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white  text-white"
          >
            Age
            
          </label>
          <div class="flex">
            <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
              A
            </span>
            <input
              type="text"
              name="nAge"
              id="website-admin"
              class="rounded-none rounded-r-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Age"
              onChange={this.handleChange}
            ></input>
          </div>
        </div>

        <div>
          <div className="flex justify-center align-center flex-col items-center">
            {/* Button Submit */}
            <button
              onClick={this.handleSubmit}
              class="bg-green-500 mt-4 hover:bg-emerald-800 text-white font-bold py-2 px-4 rounded"
            >
              Send
            </button>
            <br />
          </div>
          {/* Table for Disaply ID and Student Name */}
          <div>
            <TableComponent data={this.state.person}  avc={this.handleButton}/>
          
          </div>
        </div>
      </div>
    );
  }
}
